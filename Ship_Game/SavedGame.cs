using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using Ship_Game.AI;
using Ship_Game.AI.Tasks;
using Ship_Game.Gameplay;
using Ship_Game.Ships;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Xml.Serialization;
using Ship_Game.AI.StrategyAI.WarGoals;
using Ship_Game.Ships.AI;
using Ship_Game.Fleets;

namespace Ship_Game
{
    [AttributeUsage(AttributeTargets.Property|AttributeTargets.Field)]
    public sealed class SerializeAttribute : Attribute
    {
        public int Id { get; set; } = -1;
        public SerializeAttribute() { }
        public SerializeAttribute(int id) { Id = id; }
    }

    public sealed class HeaderData
    {
        public int SaveGameVersion;
        public string SaveName;
        public string StarDate;
        public DateTime Time;
        public string PlayerName;
        public string RealDate;
        public string ModName = "";
        public string ModPath = "";
        public int Version;

        [XmlIgnore][JsonIgnore] public FileInfo FI;
    }

    // XNA.Rectangle cannot be serialized, so we need a proxy object
    public struct RectangleData
    {
        public int X, Y, Width, Height;
        public RectangleData(Rectangle r)
        {
            X = r.X;
            Y = r.Y;
            Width = r.Width;
            Height = r.Height;
        }
        public static implicit operator Rectangle(RectangleData r)
        {
            return new Rectangle(r.X, r.Y, r.Width, r.Height);
        }
    }

    public sealed class SavedGame
    {
        // Every time the savegame layout changes significantly,
        // this version needs to be bumped to avoid loading crashes
        public const int SaveGameVersion = 8;
        public const string ZipExt = ".sav.gz";

        public readonly UniverseSaveData SaveData = new UniverseSaveData();
        public FileInfo SaveFile;
        public FileInfo PackedFile;
        public FileInfo HeaderFile;
        
        public static bool IsSaving  => GetIsSaving();
        public static bool NotSaving => !IsSaving;
        public static string DefaultSaveGameFolder => Dir.StarDriveAppData + "/Saved Games/";

        static TaskResult SaveTask;
        readonly UniverseScreen Screen;

        public SavedGame(UniverseScreen screenToSave)
        {
            Screen = screenToSave;

            // clean up and submit objects before saving
            screenToSave.Objects.UpdateLists(removeInactiveObjects: true);

            SaveData.SaveGameVersion       = SaveGameVersion;
            SaveData.gameDifficulty        = CurrentGame.Difficulty;
            SaveData.GalaxySize            = CurrentGame.GalaxySize;
            SaveData.StarsModifier         = CurrentGame.StarsModifier;
            SaveData.ExtraPlanets          = CurrentGame.ExtraPlanets;
            SaveData.AutoColonize          = EmpireManager.Player.AutoColonize;
            SaveData.AutoExplore           = EmpireManager.Player.AutoExplore;
            SaveData.AutoFreighters        = EmpireManager.Player.AutoFreighters;
            SaveData.AutoPickBestFreighter = EmpireManager.Player.AutoPickBestFreighter;
            SaveData.AutoPickBestColonizer = EmpireManager.Player.AutoPickBestColonizer;
            SaveData.AutoProjectors        = EmpireManager.Player.AutoBuild;
            SaveData.GamePacing            = CurrentGame.Pace;
            SaveData.GameScale             = 1f;
            SaveData.StarDate              = screenToSave.StarDate;
            SaveData.FTLModifier           = screenToSave.FTLModifier;
            SaveData.EnemyFTLModifier      = screenToSave.EnemyFTLModifier;
            SaveData.GravityWells          = screenToSave.GravityWells;
            SaveData.PlayerLoyalty         = screenToSave.PlayerLoyalty;
            SaveData.RandomEvent           = RandomEventManager.ActiveEvent;
            SaveData.CamPos                = screenToSave.CamPos.ToVec3f();
            SaveData.MinimumWarpRange      = GlobalStats.MinimumWarpRange;
            SaveData.TurnTimer             = (byte)GlobalStats.TurnTimer;
            SaveData.IconSize              = GlobalStats.IconSize;
            SaveData.preventFederations    = GlobalStats.PreventFederations;
            SaveData.GravityWellRange      = GlobalStats.GravityWellRange;
            SaveData.EliminationMode       = GlobalStats.EliminationMode;
            SaveData.EmpireDataList        = new Array<EmpireSaveData>();
            SaveData.SolarSystemDataList   = new Array<SolarSystemSaveData>();
            SaveData.CustomMineralDecay    = GlobalStats.CustomMineralDecay;
            SaveData.VolcanicActivity      = GlobalStats.VolcanicActivity;
            SaveData.UsePlayerDesigns      = GlobalStats.UsePlayerDesigns;
            SaveData.UseUpkeepByHullSize   = GlobalStats.UseUpkeepByHullSize;

            SaveData.SuppressOnBuildNotifications  = GlobalStats.SuppressOnBuildNotifications;
            SaveData.PlanetScreenHideOwned         = GlobalStats.PlanetScreenHideOwned;;
            SaveData.PlanetsScreenHideUnhabitable  = GlobalStats.PlanetsScreenHideUnhabitable;
            SaveData.OptionIncreaseShipMaintenance = GlobalStats.ShipMaintenanceMulti;
            SaveData.ShipListFilterPlayerShipsOnly = GlobalStats.ShipListFilterPlayerShipsOnly;
            SaveData.ShipListFilterInFleetsOnly    = GlobalStats.ShipListFilterInFleetsOnly;
            SaveData.ShipListFilterNotInFleets     = GlobalStats.ShipListFilterNotInFleets;
            SaveData.DisableInhibitionWarning      = GlobalStats.DisableInhibitionWarning;
            SaveData.CordrazinePlanetCaptured      = GlobalStats.CordrazinePlanetCaptured;
            SaveData.DisableVolcanoWarning         = GlobalStats.DisableVolcanoWarning;
            
            foreach (SolarSystem system in UniverseScreen.SolarSystemList)
            {
                SaveData.SolarSystemDataList.Add(new SolarSystemSaveData
                {
                    Name           = system.Name,
                    guid           = system.guid,
                    Position       = system.Position,
                    SunPath        = system.Sun.Id,
                    AsteroidsList  = system.AsteroidsList.Clone(),
                    Moons          = system.MoonList.Clone(),
                    ExploredBy     = system.ExploredByEmpires.Select(e => e.data.Traits.Name),
                    RingList       = system.RingList.Select(ring => ring.Serialize()),
                    PiratePresence = system.PiratePresence
                });
            }

            foreach (Empire e in EmpireManager.Empires)
            {
                var empireToSave = new EmpireSaveData
                {
                    IsFaction = e.isFaction,
                    Relations = new Array<Relationship>()
                };
                foreach (OurRelationsToThem relation in e.AllRelations)
                {
                    empireToSave.Relations.Add(relation.Rel);
                }
                empireToSave.Name                 = e.data.Traits.Name;
                empireToSave.empireData           = e.data.GetClone();
                empireToSave.Traits               = e.data.Traits;
                empireToSave.ResearchTopic        = e.Research.Topic;
                empireToSave.Money                = e.Money;
                empireToSave.CurrentAutoScout     = e.data.CurrentAutoScout;
                empireToSave.CurrentAutoFreighter = e.data.CurrentAutoFreighter;
                empireToSave.CurrentAutoColony    = e.data.CurrentAutoColony;
                empireToSave.CurrentConstructor   = e.data.CurrentConstructor;
                empireToSave.OwnedShips           = new Array<ShipSaveData>();
                empireToSave.TechTree             = new Array<TechEntry>();
                empireToSave.NormalizedMoneyVal      = e.NormalizedMoney;
                empireToSave.FastVsBigFreighterRatio   = e.FastVsBigFreighterRatio;
                empireToSave.AverageFreighterCargoCap  = e.AverageFreighterCargoCap;
                empireToSave.AverageFreighterFTLSpeed  = e.AverageFreighterFTLSpeed;
                empireToSave.ExpandSearchTimer         = e.GetEmpireAI().ExpansionAI.ExpandSearchTimer;
                empireToSave.MaxSystemsToCheckedDiv    = e.GetEmpireAI().ExpansionAI.MaxSystemsToCheckedDiv;
                empireToSave.WeightedCenter            = e.WeightedCenter;
                empireToSave.RushAllConstruction       = e.RushAllConstruction;
                empireToSave.FleetStrEmpireModifier    = e.FleetStrEmpireMultiplier;
                empireToSave.DiplomacyContactQueue     = e.DiplomacyContactQueue;
                empireToSave.ObsoletePlayerShipModules = e.ObsoletePlayerShipModules;
                empireToSave.CapitalGuid               = e.Capital?.guid ?? Guid.Empty;

                if (e.WeArePirates)
                {
                    empireToSave.PirateLevel         = e.Pirates.Level;
                    empireToSave.PirateThreatLevels  = e.Pirates.ThreatLevels;
                    empireToSave.PiratePaymentTimers = e.Pirates.PaymentTimers;
                    empireToSave.SpawnedShips        = e.Pirates.SpawnedShips;
                    empireToSave.ShipsWeCanSpawn     = e.Pirates.ShipsWeCanSpawn;
                }

                if (e.WeAreRemnants)
                {
                    empireToSave.RemnantStoryActivated      = e.Remnants.Activated;
                    empireToSave.RemnantStoryTriggerKillsXp = e.Remnants.StoryTriggerKillsXp;
                    empireToSave.RemnantStoryType           = (int)e.Remnants.Story;
                    empireToSave.RemnantProduction          = e.Remnants.Production;
                    empireToSave.RemnantLevel               = e.Remnants.Level;
                    empireToSave.RemnantStoryStep           = e.Remnants.StoryStep;
                    empireToSave.RemnantPlayerStepTriggerXp = e.Remnants.PlayerStepTriggerXp;
                    empireToSave.OnlyRemnantLeft            = e.Remnants.OnlyRemnantLeft;
                    empireToSave.RemnantNextLevelUpDate     = e.Remnants.NextLevelUpDate;
                    empireToSave.RemnantHibernationTurns    = e.Remnants.HibernationTurns;
                    empireToSave.RemnantActivationXpNeeded  = e.Remnants.ActivationXpNeeded;
                }

                foreach (AO area in e.GetEmpireAI().AreasOfOperations)
                {
                    area.PrepareForSave();
                }
                empireToSave.AOs = e.GetEmpireAI().AreasOfOperations;
                empireToSave.FleetsList = new Array<FleetSave>();
                foreach (KeyValuePair<int, Fleet> fleet in e.GetFleetsDict())
                {
                    if (fleet.Value.DataNodes == null) continue;
                    var fs = new FleetSave
                    {
                        Name            = fleet.Value.Name,
                        IsCoreFleet     = fleet.Value.IsCoreFleet,
                        TaskStep        = fleet.Value.TaskStep,
                        Key             = fleet.Key,
                        facing          = fleet.Value.FinalDirection.ToRadians(), // @note Save game compatibility uses radians
                        FleetGuid       = fleet.Value.Guid,
                        Position        = fleet.Value.FinalPosition,
                        ShipsInFleet    = new Array<FleetShipSave>(),
                        AutoRequisition = fleet.Value.AutoRequisition
                    };                    
                    foreach (FleetDataNode node in fleet.Value.DataNodes)
                    {
                        // only save ships that are currently alive (race condition when saving during intense battles)
                        if (node.Ship != null && node.Ship.Active)
                            node.ShipGuid = node.Ship.guid;
                    }
                    fs.DataNodes = fleet.Value.DataNodes;
                    foreach (Ship ship in fleet.Value.Ships)
                    {
                        fs.ShipsInFleet.Add(new FleetShipSave
                        {
                            fleetOffset = ship.RelativeFleetOffset,
                            shipGuid = ship.guid
                        });
                    }
                    empireToSave.FleetsList.Add(fs);
                }
                empireToSave.SpaceRoadData = new Array<SpaceRoadSave>();
                foreach (SpaceRoad road in e.SpaceRoadsList)
                {
                    var rdata = new SpaceRoadSave
                    {
                        OriginGUID = road.Origin.guid,
                        DestGUID = road.Destination.guid,
                        RoadNodes = new Array<RoadNodeSave>()
                    };
                    foreach (RoadNode node in road.RoadNodesList)
                    {
                        var ndata = new RoadNodeSave { Position = node.Position };
                        if (node.Platform != null) ndata.Guid_Platform = node.Platform.guid;
                        rdata.RoadNodes.Add(ndata);
                    }
                    empireToSave.SpaceRoadData.Add(rdata);
                }
                var gsaidata = new GSAISAVE
                {
                    UsedFleets = e.GetEmpireAI().UsedFleets
                };

                e.GetEmpireAI().ThreatMatrix.WriteToSave(gsaidata);
                e.GetEmpireAI().WriteToSave(gsaidata);

                Array<Goal> goals = e.GetEmpireAI().Goals;
                gsaidata.Goals = goals.Select(g =>
                {
                    var gdata = new GoalSave
                    {
                        BuildPosition = g.BuildPosition,
                        GoalStep      = g.Step,
                        ToBuildUID    = g.ToBuildUID,
                        type          = g.type,
                        GoalGuid      = g.guid,
                        GoalName      = g.UID,
                        ShipLevel     = g.ShipLevel,
                        VanityName    = g.VanityName,
                        TetherTarget  = g.TetherTarget,
                        TetherOffset  = g.TetherOffset,
                        StarDateAdded = g.StarDateAdded
                    };
                    if (g.FinishedShip != null)       gdata.colonyShipGuid            = g.FinishedShip.guid;
                    if (g.ColonizationTarget != null) gdata.markedPlanetGuid          = g.ColonizationTarget.guid;
                    if (g.PlanetBuildingAt != null)   gdata.planetWhereBuildingAtGuid = g.PlanetBuildingAt.guid;
                    if (g.TargetSystem != null)       gdata.TargetSystemGuid          = g.TargetSystem.guid;
                    if (g.TargetPlanet != null)       gdata.TargetPlanetGuid          = g.TargetPlanet.guid;
                    if (g.Fleet != null)              gdata.fleetGuid                 = g.Fleet.Guid;
                    if (g.OldShip != null)            gdata.OldShipGuid               = g.OldShip.guid;
                    if (g.TargetShip != null)         gdata.TargetShipGuid            = g.TargetShip.guid;
                    if (g.TargetEmpire != null)       gdata.TargetEmpireId            = g.TargetEmpire.Id;

                    return gdata;
                });
                empireToSave.GSAIData = gsaidata;

                empireToSave.TechTree.AddRange(e.TechEntries.ToArray());
                var ships = e.OwnedShips;
                foreach (Ship ship in ships)
                    empireToSave.OwnedShips.Add(ShipSaveFromShip(ship));

                var projectors = e.GetProjectors();
                foreach (Ship ship in projectors)  //fbedard
                    empireToSave.OwnedShips.Add(ProjectorSaveFromShip(ship));

                SaveData.EmpireDataList.Add(empireToSave);
            }

            SaveData.Projectiles = screenToSave.Objects.GetProjectileSaveData();
            SaveData.Beams       = screenToSave.Objects.GetBeamSaveData();

            SaveData.Snapshots = new SerializableDictionary<string, SerializableDictionary<int, Snapshot>>();
            foreach (KeyValuePair<string, SerializableDictionary<int, Snapshot>> e in StatTracker.SnapshotsMap)
            {
                SaveData.Snapshots.Add(e.Key, e.Value);
            }
        }

        static bool GetIsSaving()
        {
            if (SaveTask == null)
                return false;
            if (!SaveTask.IsComplete)
                return true;

            SaveTask = null; // avoids some nasty memory leak issues
            return false;
        }

        public void Save(string saveAs, bool async)
        {
            SaveData.Size = new Vector2(Screen.UniverseSize);
            SaveData.path = Dir.StarDriveAppData;
            SaveData.SaveAs = saveAs;

            string destFolder = DefaultSaveGameFolder;
            SaveFile = new FileInfo($"{destFolder}{saveAs}.sav");
            PackedFile = new FileInfo(SaveFile.FullName + ".gz");
            HeaderFile = new FileInfo($"{destFolder}Headers/{saveAs}.json");

            // FogMap is converted to a Base64 string so that it can be included in the savegame
            var exporter = Screen.ContentManager.RawContent.TexExport;
            SaveData.FogMapBase64 = exporter.ToBase64StringAlphaOnly(Screen.FogMap);

            // All of this data can be serialized in parallel,
            // because we already built `SaveData` object, which no longer depends on UniverseScreen
            SaveTask = Parallel.Run(() =>
            {
                SaveUniverseData(SaveData, SaveFile, PackedFile, HeaderFile);
            });
            
            // for blocking calls, just wait on the task
            if (!async)
                SaveTask.Wait();
        }

        public static ShipSaveData ShipSaveFromShip(Ship ship)
        {
            var sdata = new ShipSaveData(ship);
            if (ship.GetTether() != null)
            {
                sdata.TetheredTo = ship.GetTether().guid;
                sdata.TetherOffset = ship.TetherOffset;
            }
            sdata.Name = ship.Name;
            sdata.VanityName = ship.VanityName;
            sdata.Hull = ship.shipData.Hull;
            sdata.Power = ship.PowerCurrent;
            sdata.Ordnance = ship.Ordinance;
            sdata.yRotation = ship.yRotation;
            sdata.Rotation = ship.Rotation;
            sdata.InCombat = ship.InCombat;
            sdata.FoodCount = ship.GetFood();
            sdata.ProdCount = ship.GetProduction();
            sdata.PopCount = ship.GetColonists();
            sdata.TroopList = ship.GetFriendlyAndHostileTroops();
            sdata.FightersLaunched = ship.Carrier.FightersLaunched;
            sdata.TroopsLaunched = ship.Carrier.TroopsLaunched;
            sdata.SendTroopsToShip = ship.Carrier.SendTroopsToShip;
            sdata.AreaOfOperation = ship.AreaOfOperation.Select(r => new RectangleData(r));

            sdata.RecallFightersBeforeFTL = ship.Carrier.RecallFightersBeforeFTL;
            sdata.MechanicalBoardingDefense = ship.MechanicalBoardingDefense;
            sdata.OrdnanceInSpace = ship.Carrier.OrdnanceInSpace;
            sdata.ScuttleTimer = ship.ScuttleTimer;

            if (ship.IsHomeDefense)
                sdata.HomePlanetGuid = ship.HomePlanet.guid;

            if (ship.TradeRoutes?.NotEmpty == true)
            {
                sdata.TradeRoutes = new Array<Guid>();
                foreach (Guid planetGuid in ship.TradeRoutes)
                {
                    sdata.TradeRoutes.Add(planetGuid);
                }
            }

            sdata.TransportingFood = ship.TransportingFood;
            sdata.TransportingProduction = ship.TransportingProduction;
            sdata.TransportingColonists = ship.TransportingColonists;
            sdata.AllowInterEmpireTrade = ship.AllowInterEmpireTrade;
            sdata.AISave = new ShipAISave
            {
                State = ship.AI.State,
                DefaultState = ship.AI.DefaultAIState,
                CombatState = ship.AI.CombatState,
                StateBits = ship.AI.StateBits,
            };
            if (ship.AI.Target is Ship targetShip)
            {
                sdata.AISave.AttackTarget = targetShip.guid;
            }
            sdata.AISave.MovePosition = ship.AI.MovePosition;
            sdata.AISave.WayPoints = new Array<WayPoint>(ship.AI.CopyWayPoints());
            sdata.AISave.ShipGoalsList = new Array<ShipGoalSave>();

            foreach (ShipAI.ShipGoal sg in ship.AI.OrderQueue)
            {
                var s = new ShipGoalSave
                {
                    Plan             = sg.Plan,
                    Direction        = sg.Direction,
                    VariableString   = sg.VariableString,
                    SpeedLimit       = sg.SpeedLimit,
                    MovePosition     = sg.MovePosition,
                    fleetGuid        = sg.Fleet?.Guid ?? Guid.Empty,
                    goalGuid         = sg.Goal?.guid ?? Guid.Empty,
                    TargetPlanetGuid = sg.TargetPlanet?.guid ?? Guid.Empty,
                    TargetShipGuid   = sg.TargetShip?.guid ?? Guid.Empty,
                    MoveType         = sg.MoveType,
                    VariableNumber   = sg.VariableNumber,
                    WantedState      = sg.WantedState
                };

                if (sg.Trade != null)
                {
                    s.Trade = new TradePlanSave
                    {
                        Goods         = sg.Trade.Goods,
                        ExportFrom    = sg.Trade.ExportFrom?.guid ?? Guid.Empty,
                        ImportTo      = sg.Trade.ImportTo?.guid ?? Guid.Empty,
                        BlockadeTimer = sg.Trade.BlockadeTimer,
                        StardateAdded = sg.Trade.StardateAdded
                    };
                }
                sdata.AISave.ShipGoalsList.Add(s);
            }

            if (ship.AI.OrbitTarget != null)
                sdata.AISave.OrbitTarget = ship.AI.OrbitTarget.guid;

            if (ship.AI.SystemToDefend != null)
                sdata.AISave.SystemToDefend = ship.AI.SystemToDefend.guid;

            if (ship.AI.EscortTarget != null)
                sdata.AISave.EscortTarget = ship.AI.EscortTarget.guid;
            return sdata;
        }

        public static ShipSaveData ProjectorSaveFromShip(Ship ship)
        {
            var sd = new ShipSaveData(ship);
            if (ship.GetTether() != null)
            {
                sd.TetheredTo = ship.GetTether().guid;
                sd.TetherOffset = ship.TetherOffset;
            }
            sd.Name = ship.Name;
            sd.VanityName = ship.VanityName;
            sd.Hull      = ship.shipData.Hull;
            sd.Power     = ship.PowerCurrent;
            sd.Ordnance  = ship.Ordinance;
            sd.yRotation = ship.yRotation;
            sd.Rotation  = ship.Rotation;
            sd.InCombat  = ship.InCombat;
            sd.AISave = new ShipAISave
            {
                State           = ship.AI.State,
                DefaultState    = ship.AI.DefaultAIState,
                MovePosition    = ship.AI.MovePosition,
                WayPoints       = new Array<WayPoint>(),
                ShipGoalsList   = new Array<ShipGoalSave>()
            };
            return sd;
        }

        static void SaveUniverseData(UniverseSaveData data, FileInfo saveFile, 
                                     FileInfo compressedSave, FileInfo headerFile)
        {
            var t = new PerfTimer();
            using (var textWriter = new StreamWriter(saveFile.FullName))
            {
                var ser = new JsonSerializer
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    DefaultValueHandling = DefaultValueHandling.Ignore
                };
                ser.Serialize(textWriter, data);
            }
            Log.Warning($"JSON Total Save elapsed: {t.Elapsed}s");

            HelperFunctions.Compress(saveFile, compressedSave); // compress into .sav.gz
            saveFile.Delete(); // delete the bigger .sav file

            // Save the header as well
            DateTime now = DateTime.Now;
            var header = new HeaderData
            {
                SaveGameVersion = SaveGameVersion,
                PlayerName = data.PlayerLoyalty,
                StarDate   = data.StarDate.ToString("#.0"),
                Time       = now,
                SaveName   = data.SaveAs,
                RealDate   = now.ToString("M/d/yyyy") + " " + now.ToString("t", CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat),
                ModPath    = GlobalStats.ActiveMod?.ModName    ?? "",
                ModName    = GlobalStats.ActiveMod?.mi.ModName ?? "",
                Version    = Convert.ToInt32(ConfigurationManager.AppSettings["SaveVersion"])
            };

            using (var textWriter = new StreamWriter(headerFile.FullName))
            {
                var ser = new JsonSerializer
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    DefaultValueHandling = DefaultValueHandling.Ignore
                };
                ser.Serialize(textWriter, header);
            }

            SaveTask = null;

            HelperFunctions.CollectMemory();
        }

        public static UniverseSaveData DeserializeFromCompressedSave(FileInfo compressedSave)
        {
            UniverseSaveData usData;
            var decompressed = new FileInfo(HelperFunctions.Decompress(compressedSave));

            var t = new PerfTimer();
            using (FileStream stream = decompressed.OpenRead())
            using (var reader = new JsonTextReader(new StreamReader(stream)))
            {
                var ser = new JsonSerializer
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    DefaultValueHandling = DefaultValueHandling.Ignore
                };
                usData = ser.Deserialize<UniverseSaveData>(reader);
            }

            Log.Warning($"JSON Total Load elapsed: {t.Elapsed}s  ");
            decompressed.Delete();

            //HelperFunctions.CollectMemory();
            return usData;
        }

        public class EmpireSaveData
        {
            [Serialize(0)] public string Name;
            [Serialize(1)] public Array<Relationship> Relations;
            [Serialize(2)] public Array<SpaceRoadSave> SpaceRoadData;
            [Serialize(3)] public bool IsFaction;
            [Serialize(5)] public RacialTrait Traits;
            [Serialize(6)] public EmpireData empireData;
            [Serialize(7)] public Array<ShipSaveData> OwnedShips;
            [Serialize(8)] public float Money;
            [Serialize(9)] public Array<TechEntry> TechTree;
            [Serialize(10)] public GSAISAVE GSAIData;
            [Serialize(11)] public string ResearchTopic;
            [Serialize(12)] public Array<AO> AOs;
            [Serialize(13)] public Array<FleetSave> FleetsList;
            [Serialize(14)] public string CurrentAutoFreighter;
            [Serialize(15)] public string CurrentAutoColony;
            [Serialize(16)] public string CurrentAutoScout;
            [Serialize(17)] public string CurrentConstructor;
            [Serialize(18)] public float FastVsBigFreighterRatio;
            [Serialize(19)] public float AverageFreighterCargoCap;
            [Serialize(20)] public int PirateLevel;
            [Serialize(21)] public Map<int, int> PirateThreatLevels;
            [Serialize(22)] public Map<int, int> PiratePaymentTimers;
            [Serialize(23)] public Array<Guid> SpawnedShips;
            [Serialize(24)] public Array<string> ShipsWeCanSpawn;
            [Serialize(25)] public float NormalizedMoneyVal;
            [Serialize(26)] public int ExpandSearchTimer;
            [Serialize(27)] public int MaxSystemsToCheckedDiv;
            [Serialize(28)] public int AverageFreighterFTLSpeed;
            [Serialize(29)] public Vector2 WeightedCenter;
            [Serialize(30)] public bool RushAllConstruction;
            [Serialize(31)] public float RemnantStoryTriggerKillsXp;
            [Serialize(32)] public bool RemnantStoryActivated;
            [Serialize(33)] public int RemnantStoryType;
            [Serialize(34)] public float RemnantProduction;
            [Serialize(35)] public int RemnantLevel;
            [Serialize(36)] public int RemnantStoryStep;
            [Serialize(37)] public float RemnantPlayerStepTriggerXp;
            [Serialize(38)] public bool OnlyRemnantLeft;
            [Serialize(39)] public float RemnantNextLevelUpDate;
            [Serialize(40)] public int RemnantHibernationTurns;
            [Serialize(41)] public float RemnantActivationXpNeeded;
            [Serialize(42)] public Map<int, float> FleetStrEmpireModifier;
            [Serialize(43)] public List<KeyValuePair<int, string>> DiplomacyContactQueue;
            [Serialize(44)] public Array<string> ObsoletePlayerShipModules;
            [Serialize(45)] public Guid CapitalGuid;
        }

        public class FleetSave
        {
            [Serialize(0)] public bool IsCoreFleet;
            [Serialize(1)] public string Name;
            [Serialize(2)] public int TaskStep;
            [Serialize(3)] public Vector2 Position;
            [Serialize(4)] public Guid FleetGuid;
            [Serialize(5)] public float facing;
            [Serialize(6)] public int Key;
            [Serialize(7)] public Array<FleetShipSave> ShipsInFleet;
            [Serialize(8)] public Array<FleetDataNode> DataNodes;
            [Serialize(9)] public bool AutoRequisition;

            public override string ToString() => $"FleetSave {Name} (core={IsCoreFleet}) {FleetGuid} {Position}";
        }

        public struct FleetShipSave
        {
            [Serialize(0)] public Guid shipGuid;
            [Serialize(1)] public Vector2 fleetOffset;

            public override string ToString() => $"FleetShipSave {shipGuid} {fleetOffset}";
        }

        public class GoalSave
        {
            [Serialize(0)] public GoalType type;
            [Serialize(1)] public int GoalStep;
            [Serialize(2)] public Guid markedPlanetGuid; // @note renamed to: Goal.ColonizationTarget
            [Serialize(3)] public Guid colonyShipGuid;   // @note renamed to: Goal.FinishedShip
            [Serialize(4)] public Vector2 BuildPosition;
            [Serialize(5)] public string ToBuildUID;
            [Serialize(6)] public Guid planetWhereBuildingAtGuid;
            [Serialize(7)] public string GoalName;
            [Serialize(9)] public Guid fleetGuid;
            [Serialize(10)] public Guid GoalGuid;
            [Serialize(11)] public Guid OldShipGuid;
            [Serialize(12)] public string VanityName;
            [Serialize(13)] public int ShipLevel;
            [Serialize(14)] public Guid TetherTarget;
            [Serialize(15)] public Vector2 TetherOffset;
            [Serialize(16)] public Guid TargetShipGuid;
            [Serialize(17)] public int TargetEmpireId;
            [Serialize(18)] public float StarDateAdded;
            [Serialize(19)] public Guid TargetSystemGuid;
            [Serialize(20)] public Guid TargetPlanetGuid;
        }

        public class GSAISAVE
        {
            [Serialize(0)] public Array<int> UsedFleets;
            [Serialize(1)] public GoalSave[] Goals;
            [Serialize(2)] public Array<MilitaryTask> MilitaryTaskList;
            [Serialize(3)] public Array<Guid> PinGuids;
            [Serialize(4)] public Array<ThreatMatrix.Pin> PinList;
        }

        public class PGSData
        {
            [Serialize(0)] public int x;
            [Serialize(1)] public int y;
            [Serialize(2)] public Array<Troop> TroopsHere;
            [Serialize(3)] public bool Biosphere;
            [Serialize(4)] public Building building;
            [Serialize(5)] public bool Habitable;
            [Serialize(6)] public bool Terraformable;
            [Serialize(7)] public bool CrashSiteActive;
            [Serialize(8)] public int CrashSiteTroops;
            [Serialize(9)] public string CrashSiteShipName;
            [Serialize(10)] public string CrashSiteTroopName;
            [Serialize(11)] public int CrashSiteEmpireId;
            [Serialize(12)] public bool CrashSiteRecoverShip;
            [Serialize(13)] public short EventOutcomeNum;
            [Serialize(14)] public bool VolcanoHere;
            [Serialize(15)] public bool VolcanoActive;
            [Serialize(16)] public bool VolcanoErupting;
            [Serialize(17)] public float VolcanoActivationChance;
        }

        public class PlanetSaveData
        {
            [Serialize(0)] public Guid guid;
            [Serialize(1)] public string SpecialDescription;
            [Serialize(2)] public string Name;
            [Serialize(3)] public float Scale;
            [Serialize(4)] public string Owner;
            [Serialize(5)] public float Population;
            [Serialize(6)] public float BasePopPerTile;
            [Serialize(7)] public float Fertility;
            [Serialize(8)] public float Richness;
            [Serialize(9)] public int WhichPlanet;
            [Serialize(10)] public float OrbitalAngle;
            [Serialize(11)] public float OrbitalDistance;
            [Serialize(12)] public float Radius;
            [Serialize(13)] public bool HasRings;
            [Serialize(14)] public float farmerPercentage;
            [Serialize(15)] public float workerPercentage;
            [Serialize(16)] public float researcherPercentage;
            [Serialize(17)] public float foodHere;
            [Serialize(18)] public float prodHere;
            [Serialize(19)] public PGSData[] PGSList;
            [Serialize(20)] public QueueItemSave[] QISaveList;
            [Serialize(21)] public Planet.ColonyType ColonyType;
            [Serialize(22)] public Planet.GoodState FoodState;
            [Serialize(23)] public int Crippled_Turns;
            [Serialize(24)] public Planet.GoodState ProdState;
            [Serialize(25)] public string[] ExploredBy;
            [Serialize(26)] public float TerraformPoints;
            [Serialize(27)] public Guid[] StationsList;
            [Serialize(28)] public bool FoodLock;
            [Serialize(29)] public bool ResLock;
            [Serialize(30)] public bool ProdLock;
            [Serialize(31)] public float ShieldStrength;
            [Serialize(32)] public float MaxFertility;
            [Serialize(33)] public Guid[] IncomingFreighters;
            [Serialize(34)] public Guid[] OutgoingFreighters;
            [Serialize(35)] public bool GovOrbitals;
            [Serialize(36)] public bool GovMilitia;
            [Serialize(37)] public int NumShipyards;
            [Serialize(38)] public bool DontScrapBuildings;
            [Serialize(39)] public int GarrisonSize;
            [Serialize(40)] public float BaseFertilityTerraformRatio;
            [Serialize(41)] public bool Quarantine;
            [Serialize(42)] public bool ManualOrbitals;
            [Serialize(43)] public byte WantedPlatforms;
            [Serialize(44)] public byte WantedStations;
            [Serialize(45)] public byte WantedShipyards;
            [Serialize(46)] public bool GovGroundDefense;
            [Serialize(47)] public float ManualCivilianBudget;
            [Serialize(48)] public float ManualGrdDefBudget;
            [Serialize(49)] public float ManualSpcDefBudget;
            [Serialize(50)] public bool HasLimitedResourcesBuildings;
            [Serialize(51)] public int ManualFoodImportSlots;
            [Serialize(52)] public int ManualProdImportSlots;
            [Serialize(53)] public int ManualColoImportSlots;
            [Serialize(54)] public int ManualFoodExportSlots;
            [Serialize(55)] public int ManualProdExportSlots;
            [Serialize(56)] public int ManualColoExportSlots;
            [Serialize(57)] public float AverageFoodImportTurns;
            [Serialize(58)] public float AverageProdImportTurns;
            [Serialize(59)] public float AverageFoodExportTurns;
            [Serialize(60)] public float AverageProdExportTurns;
            [Serialize(61)] public bool IsHomeworld;
            [Serialize(62)] public int BombingIntensity;

            public override string ToString() => $"PlanetSD {Name}";
        }

        public struct ProjectileSaveData
        {
            [Serialize(0)] public Guid Owner; // Ship or Planet
            [Serialize(1)] public string Weapon;
            [Serialize(2)] public float Duration;
            [Serialize(3)] public float Rotation;
            [Serialize(4)] public Vector2 Velocity;
            [Serialize(5)] public Vector2 Position;
            [Serialize(6)] public int Loyalty;
        }

        public struct BeamSaveData
        {
            [Serialize(0)] public Guid Owner; // Ship or Planet
            [Serialize(1)] public string Weapon;
            [Serialize(2)] public float Duration;
            [Serialize(3)] public Vector2 Source;
            [Serialize(4)] public Vector2 Destination;
            [Serialize(5)] public Vector2 ActualHitDestination;
            [Serialize(6)] public Guid Target; // Ship or Projectile
            [Serialize(7)] public int Loyalty;
        }

        public class QueueItemSave
        {
            [Serialize(0)] public string UID;
            [Serialize(1)] public Guid GoalGUID;
            [Serialize(2)] public float ProgressTowards;
            [Serialize(3)] public bool isBuilding;
            [Serialize(4)] public bool isTroop;
            [Serialize(5)] public bool isShip;
            [Serialize(6)] public string DisplayName;
            [Serialize(7)] public float Cost;
            [Serialize(8)] public Vector2 pgsVector;
            [Serialize(9)] public bool isPlayerAdded;
            [Serialize(10)] public Array<Guid> TradeRoutes;
            [Serialize(11)] public RectangleData[] AreaOfOperation;
            [Serialize(12)] public bool TransportingColonists;
            [Serialize(13)] public bool TransportingFood;
            [Serialize(14)] public bool TransportingProduction;
            [Serialize(15)] public bool AllowInterEmpireTrade;
            [Serialize(16)] public bool IsMilitary;
            [Serialize(17)] public bool Rush;
        }

        public struct RingSave
        {
            [Serialize(0)] public PlanetSaveData Planet;
            [Serialize(1)] public bool Asteroids;
            [Serialize(2)] public float OrbitalDistance;

            public override string ToString() => $"RingSave {OrbitalDistance} Ast:{Asteroids} {Planet}";
        }

        public struct RoadNodeSave
        {
            [Serialize(0)] public Vector2 Position;
            [Serialize(1)] public Guid Guid_Platform;
        }

        public class ShipAISave
        {
            [Serialize(0)] public AIState State;
            [Serialize(1)] public AIState DefaultState;
            [Serialize(2)] public CombatState CombatState;
            [Serialize(3)] public ShipAI.Flags StateBits;
            [Serialize(4)] public Array<ShipGoalSave> ShipGoalsList;
            [Serialize(5)] public Array<WayPoint> WayPoints;
            [Serialize(6)] public Vector2 MovePosition;
            [Serialize(7)] public Guid OrbitTarget;
            [Serialize(8)] public Guid SystemToDefend;
            [Serialize(9)] public Guid AttackTarget;
            [Serialize(10)] public Guid EscortTarget;
        }

        public class ShipGoalSave
        {
            [Serialize(0)] public ShipAI.Plan Plan;
            [Serialize(1)] public Guid goalGuid;
            [Serialize(2)] public string VariableString;
            [Serialize(3)] public Guid fleetGuid;
            [Serialize(4)] public float SpeedLimit;
            [Serialize(5)] public Vector2 MovePosition;
            [Serialize(6)] public Vector2 Direction;
            [Serialize(7)] public Guid TargetPlanetGuid;
            [Serialize(8)] public TradePlanSave Trade;
            [Serialize(9)] public AIState WantedState;
            [Serialize(10)] public Guid TargetShipGuid;
            [Serialize(11)] public ShipAI.MoveTypes MoveType;
            [Serialize(12)] public float VariableNumber;

            public override string ToString()
            {
                return $"SGSave {Plan} MP={MovePosition} TS={TargetShipGuid} TP={TargetPlanetGuid}";
            }
        }

        public class TradePlanSave
        {
            [Serialize(0)] public Guid ExportFrom;
            [Serialize(1)] public Guid ImportTo;
            [Serialize(2)] public Goods Goods;
            [Serialize(3)] public float BlockadeTimer;
            [Serialize(4)] public float StardateAdded;
        }

        public class ShipSaveData
        {
            [Serialize(0)] public Guid GUID;
            [Serialize(1)] public bool AfterBurnerOn;
            [Serialize(2)] public ShipAISave AISave;
            [Serialize(3)] public Vector2 Position;
            [Serialize(4)] public Vector2 Velocity;
            [Serialize(5)] public float Rotation;
            // 200 IQ solution: store a base64 string of the ship module saves
            // and avoid a bunch of annoying serialization issues
            [Serialize(6)] public string ModulesBase64;
            [Serialize(7)] public string Hull; // ShipHull name
            [Serialize(8)] public string Name; // ShipData design name
            [Serialize(9)] public string VanityName; // User defined name
            [Serialize(10)] public float yRotation;
            [Serialize(11)] public float Power;
            [Serialize(12)] public float Ordnance;
            [Serialize(13)] public bool InCombat;
            [Serialize(13)] public float BaseStrength;
            [Serialize(13)] public int Level;
            [Serialize(14)] public float Experience;
            [Serialize(15)] public int Kills;
            [Serialize(16)] public Array<Troop> TroopList;
            [Serialize(17)] public RectangleData[] AreaOfOperation;
            [Serialize(18)] public float FoodCount;
            [Serialize(19)] public float ProdCount;
            [Serialize(20)] public float PopCount;
            [Serialize(21)] public Guid TetheredTo;
            [Serialize(22)] public Vector2 TetherOffset;
            [Serialize(23)] public bool FightersLaunched;
            [Serialize(24)] public bool TroopsLaunched;
            [Serialize(25)] public Guid HomePlanetGuid;
            [Serialize(26)] public bool TransportingFood;
            [Serialize(27)] public bool TransportingProduction;
            [Serialize(28)] public bool TransportingColonists;
            [Serialize(29)] public bool AllowInterEmpireTrade;
            [Serialize(30)] public Array<Guid> TradeRoutes;
            [Serialize(31)] public bool SendTroopsToShip;
            [Serialize(32)] public bool RecallFightersBeforeFTL;
            [Serialize(33)] public float MechanicalBoardingDefense;
            [Serialize(34)] public float OrdnanceInSpace; // For carriers
            [Serialize(35)] public float ScuttleTimer = -1;

            public ShipSaveData() {}

            public ShipSaveData(Ship ship)
            {
                Name = ship.Name;
                MechanicalBoardingDefense = ship.MechanicalBoardingDefense;
                GUID = ship.guid;
                Position   = ship.Position;

                BaseStrength = ship.BaseStrength;
                Level      = ship.Level;
                Experience = ship.experience;
                Kills      = ship.kills;
                Velocity   = ship.Velocity;

                ModulesBase64 = ShipDesign.GetBase64ModulesString(ship);
            }

            public override string ToString() => $"ShipSave {GUID} {Name}";
        }

        public class SolarSystemSaveData
        {
            [Serialize(0)] public Guid guid;
            [Serialize(1)] public string SunPath; // old SunPath is actually the ID @todo RENAME
            [Serialize(2)] public string Name;
            [Serialize(3)] public Vector2 Position;
            [Serialize(4)] public RingSave[] RingList;
            [Serialize(5)] public Array<Asteroid> AsteroidsList;
            [Serialize(6)] public Array<Moon> Moons;
            [Serialize(7)] public string[] ExploredBy;
            [Serialize(8)] public bool PiratePresence;

            public override string ToString()
            {
                return $"{SunPath} {Name} {Position} "+
                       $"Rings:{RingList.Length} Ast:{AsteroidsList.Count} "+
                       $"Moons:{Moons.Count} Pirates:{PiratePresence} ExploredBy:{string.Join(",",ExploredBy)}";
            }
        }

        public struct SpaceRoadSave
        {
            [Serialize(0)] public Array<RoadNodeSave> RoadNodes;
            [Serialize(1)] public Guid OriginGUID;
            [Serialize(2)] public Guid DestGUID;
        }

        public class UniverseSaveData
        {
            public int SaveGameVersion;
            public string path;
            public string SaveAs;
            public string FileName;
            public string FogMapBase64;
            public string PlayerLoyalty;
            public Vector3 CamPos;
            public Vector2 Size;
            public float StarDate;
            public float GameScale;
            public float GamePacing;
            public Array<SolarSystemSaveData> SolarSystemDataList;
            public Array<EmpireSaveData> EmpireDataList;
            public UniverseData.GameDifficulty gameDifficulty;
            public bool AutoExplore;
            public bool AutoColonize;
            public bool AutoFreighters;
            public bool AutoProjectors;
            public float FTLModifier = 1.0f;
            public float EnemyFTLModifier = 1.0f;
            public bool GravityWells;
            public RandomEvent RandomEvent;
            public SerializableDictionary<string, SerializableDictionary<int, Snapshot>> Snapshots;
            public float OptionIncreaseShipMaintenance = GlobalStats.ShipMaintenanceMulti;
            public float MinimumWarpRange = GlobalStats.MinimumWarpRange;
            public int IconSize;
            public byte TurnTimer;
            public bool preventFederations;
            public float GravityWellRange = GlobalStats.GravityWellRange;
            public bool EliminationMode;
            public bool AutoPickBestFreighter;
            public GalSize GalaxySize;
            public float StarsModifier = 1;
            public int ExtraPlanets;
            public ProjectileSaveData[] Projectiles; // New global projectile list
            public BeamSaveData[] Beams; // new global beam list
            public bool AutoPickBestColonizer;
            public float CustomMineralDecay;
            public bool SuppressOnBuildNotifications;
            public bool PlanetScreenHideOwned;
            public bool PlanetsScreenHideUnhabitable;
            public bool ShipListFilterPlayerShipsOnly;
            public bool ShipListFilterInFleetsOnly;
            public bool ShipListFilterNotInFleets;
            public bool DisableInhibitionWarning;
            public bool CordrazinePlanetCaptured;
            public bool DisableVolcanoWarning;
            public float VolcanicActivity;
            public bool UsePlayerDesigns;
            public bool UseUpkeepByHullSize;
        }
    }
}
