namespace Ship_Game.Ships
{
    public enum ShipCategory
    {
        Unclassified,
        Civilian,
        Recon,
        Conservative,
        Neutral,
        Reckless,
        Kamikaze
    }
}