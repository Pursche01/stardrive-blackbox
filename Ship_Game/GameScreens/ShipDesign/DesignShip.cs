﻿using Ship_Game.Gameplay;
using Ship_Game.Ships;

// ReSharper disable once CheckNamespace
namespace Ship_Game.GameScreens.ShipDesign
{
    public class DesignShip : Ship
    {
        DesignSlot[] PrevSlots;

        public ShipDesignStats DesignStats;

        public DesignShip(Ships.ShipDesign designHull)
            : base(EmpireManager.Player, designHull, isTemplate:true, shipyardDesign:true)
        {
            DesignStats = new ShipDesignStats(this);
        }

        public void UpdateDesign(DesignSlot[] placedSlots)
        {
            if (PrevSlots != null && AreEqual(PrevSlots, placedSlots))
                return;

            PrevSlots = placedSlots;
            shipData.SetDesignSlots(placedSlots);
            CreateModuleSlotsFromData(placedSlots, isTemplate:true, shipyardDesign:true);
            InitializeShip();
            DesignStats.Update();
        }

        static bool AreEqual(DesignSlot[] slotsA, DesignSlot[] slotsB)
        {
            if (slotsA.Length != slotsB.Length)
                return false;
            for (int i = 0; i < slotsA.Length; ++i)
                if (!slotsA[i].Equals(slotsB[i]))
                    return false;
            return true;
        }
    }
}
